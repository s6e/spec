OpenHLA Specification
=====================

Version 0.0.1
-------------

The key words \"MUST\", \"MUST NOT\", \"REQUIRED\", \"SHALL\", \"SHALL
NOT\", \"SHOULD\", \"SHOULD NOT\", \"RECOMMENDED\", \"NOT RECOMMENDED\",
\"MAY\", and \"OPTIONAL\" in this document are to be interpreted as
described in BCP 14 RFC2119 RFC8174 when, and only when, they appear in
all capitals, as shown here.

This document is licensed under The Apache License, Version 2.0.

**INFO:** This document is based on and derrived from OpenApi Specification 3.0.3.

Introduction
------------

The OpenHLA Specification (OHS) defines a standard, language-agnostic
interface to application high level architecture which allows both
humans and computers to discover and understand the structure and
dependencies of components without access to source code, documentation,
or through network traffic inspection.

Definitions
-----------

### OpenHLA Document

A document (or set of documents) that defines or describes an
application HLA. An OpenHLA definition uses and conforms to the OpenHLA
Specification.

Specification
-------------

### Versions

The OpenHLA Specification is versioned using Semantic Versioning 2.0.0
(semver) and follows the semver specification.

The major.minor portion of the semver (for example 3.0) SHALL designate
the OHS feature set. Typically, .patch versions address errors in this
document, not the feature set. Tooling which supports OHS 3.0 SHOULD be
compatible with all OHS 3.0.\* versions. The patch version SHOULD NOT be
considered by tooling, making no distinction between 3.0.0 and 3.0.1 for
example.

Each new minor version of the OpenHLA Specification SHALL allow any
OpenHLA document that is valid against any previous minor version of the
Specification, within the same major version, to be updated to the new
Specification version with equivalent semantics. Such an update MUST
only require changing the openhla property to the new minor version.

For example, a valid OpenHLA 3.0.2 document, upon changing its openhla
property to 3.1.0, SHALL be a valid OpenHLA 3.1.0 document, semantically
equivalent to the original OpenHLA 3.0.2 document. New minor versions of
the OpenHLA Specification MUST be written to ensure this form of
backward compatibility.

An OpenHLA document compatible with OHS 3.\*.\* contains a required
openhla field which designates the semantic version of the OHS that it
uses.

### Format

An OpenHLA document that conforms to the OpenHLA Specification is itself
a JSON object, which may be represented either in JSON or YAML format.

For example, if a field has an array value, the JSON array
representation will be used:

```
{
  "field": [ 1, 2, 3 ]
}
```

All field names in the specification are **case sensitive**. This
includes all fields that are used as keys in a map, except where
explicitly noted that keys are **case insensitive**.

The schema exposes two types of fields: Fixed fields, which have a
declared name, and Patterned fields, which declare a regex pattern for
the field name.

Patterned fields MUST have unique names within the containing object.

In order to preserve the ability to round-trip between YAML and JSON
formats, YAML version 1.2 is RECOMMENDED along with some additional
constraints:

-   Tags MUST be limited to those allowed by the JSON Schema ruleset.

-   Keys used in YAML maps MUST be limited to a scalar string, as
    > defined by the YAML Failsafe schema ruleset.

### Document Structure

An OpenHLA document MAY be made up of a single document or be divided
into multiple, connected parts at the discretion of the user. In the
latter case, \$ref fields MUST be used in the specification to reference
those parts as follows from the JSON Schema definitions.

It is RECOMMENDED that the root OpenHLA document be named: openhla.json
or openhla.yaml.

### Data Types

Primitive data types in the OAS are based on the types supported by the
[JSON Schema Specification Wright Draft 00](https://tools.ietf.org/html/draft-wright-json-schema-00#section-4.2).
Note that integer as a type is also supported and is defined as a JSON
number without a fraction or exponent part. null is not supported as a
type (see nullable for an alternative solution). Models are defined
using the [Schema Object],
which is an extended subset of JSON Schema Specification Wright Draft
00.

Primitives have an optional modifier property: format. OAS uses several
known formats to define in fine detail the data type being used.
However, to support documentation needs, the format property is an open
string-valued property, and can have any value. Formats such as
\"email\", \"uuid\", and so on, MAY be used even though undefined by
this specification. Types that are not accompanied by a format property
follow the type definition in the JSON Schema. Tools that do not
recognize a specific format MAY default back to the type alone, as if
the format is not specified.

The formats defined by the OAS are:

  [type]   [format]   Comments
  ------------------------------------------------------------------- -------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------
  integer                                                             int32                                                                      signed 32 bits
  integer                                                             int64                                                                      signed 64 bits (a.k.a long)
  number                                                              float                                                                      
  number                                                              double                                                                     
  string                                                                                                                                         
  string                                                              byte                                                                       base64 encoded characters
  string                                                              binary                                                                     any sequence of octets
  boolean                                                                                                                                        
  string                                                              date                                                                       As defined by full-date - [RFC3339](https://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14)
  string                                                              date-time                                                                  As defined by date-time - [RFC3339](https://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14)
  string                                                              password                                                                   A hint to UIs to obscure input.

### Rich Text Formatting

Throughout the specification description fields are noted as supporting
CommonMark markdown formatting. Where OpenHLA tooling renders rich text
it MUST support, at a minimum, markdown syntax as described by
[CommonMark 0.27](https://spec.commonmark.org/0.27/).
Tooling MAY choose to ignore some CommonMark features to address
security concerns.

### Schema

In the following description, if a field is not explicitly REQUIRED or
described with a MUST or SHALL, it can be considered OPTIONAL.

#### OpenHLA Object

#### This is the root document object of the OpenHLA document.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| openhla | string | **REQUIRED**. This string MUST be the semantic version number of the OpenHLA Specification version that the OpenHLA document uses. The openhla field SHOULD be used by tooling specifications and clients to interpret the OpenHLA document. This is not related to the HLA info.version string. |
| info | Info Object | **REQUIRED**. Provides metadata about the application. The metadata MAY be used by tooling as required. |
| components | Component Object | An element to hold various schemas for the specification. |
| tags | Tag Object | A list of tags used by the specification with additional metadata. The order of the tags can be used to reflect on their order by the parsing tools. Not all tags that are used must be declared. The tags that are not declared MAY be organized randomly or based on the tools\' logic. Each tag name in the list MUST be unique. |
| layers | Layer Object | A list of layers used by the specification with additional metadata. The order of the layers can be used to reflect on their order by the parsing tools. Not all layers that are used must be declared. The layers that are not declared MAY be organized randomly or based on the tools\' logic. Each layer name in the list MUST be unique. |
| imports | string | List of partial documents extending the platform specification. Each import can be local file path or Url. During import all tags, layers and components collections will be extended. Url Imports WILL NOT be loaded recursivelly. External docs and info will be ignored. |
| externalDocs | External Documentation Object | Additional external documentation. |

This object MAY be extended with Specification Extensions.

#### Info Object

The object provides metadata about the platform. The metadata MAY be presented in editing or documentation
generation tools for convenience.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| name | string | **REQUIRED**. The name of the platform. |
| description | string | A short description of the API. [CommonMark syntax](https://spec.commonmark.org/) MAY be used for rich text representation. |
| termsOfService | string | A URL to the Terms of Service for the API. MUST be in the format of a URL. |
| contact | Contact Object | The contact information for the exposed API. |
| license | License Object | The license information for the exposed API. |
| version | string | **REQUIRED**. The version of the OpenHLA document (which is distinct from the OpenHLA Specification version or the platform implementation version). |

This object MAY be extended with Specification
Extensions.

#### Contact Object

Contact information for the exposed API.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| name | string | The identifying name of the contact person/organization. |
| url | string | The URL pointing to the contact information. MUST be in the format of a URL. |
| email | string | The email address of the contact person/organization. MUST be in the format of an email address. |

This object MAY be extended with Specification Extensions.

#### License Object

License information for the exposed API.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| name | string | **REQUIRED**. The license name used for the API. |
| url | string | A URL to the license used for the API. MUST be in the format of a URL. |

This object MAY be extended with Specification Extensions.

#### Component Object

Holds a set of building blocks (components) for the platform.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| name | string | **REQUIRED.** Unique name of the component. |
| description | string | A short description of the component. [CommonMark syntax](https://spec.commonmark.org/) MAY be used for rich text representation. |
| type | string | Type of the object such as Service, Worker, Database, etc. |
| open\_api | \[OpenApi Object\] | Additional component API documentation. |
| eventsEmitted | \[EventEmitted Object\] | A list of events throwed by component. |
| eventsConsumed | \[EventsConsumed Object\] | A list of events consumed by component. |
| depends_on | string | A list of component names consumed by component. |
| integrations | \[Integration Object\] | A list of external integrations used by component. |
| tags | string | A list of tag names. |
| stack | string | A list of technologies used to build the component. May contain frameworks, programming languages, etc. |
| layer | string | Layer of the system. |

This object MAY be extended with Specification Extensions.

#### OpenApi Object

OpenApi documentation for Api exposed by component.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| spec\_url | string | **REQUIRED.** Url to OpenApi specification document. |
| site\_url | string | Url to Swagger documentation site. |

This object MAY be extended with Specification Extensions.

#### EventEmitted Object

Event emitted by component.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| name | string | **REQUIRED.** Unique name of the event.
| description | string | A short description of the event. [CommonMark syntax](https://spec.commonmark.org/) MAY be used for rich text representation. |
| queue | string | Queue name. |
| Payload | Payload Object | Payload description. |

This object MAY be extended with Specification Extensions.

#### EventConsumed Object

Event consumed by component.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| name | string | **REQUIRED.** Unique name of the event.
| description | string | A short description of reason for consuming the event. [CommonMark syntax](https://spec.commonmark.org/) MAY be used for rich text representation. |
| queue | string | Queue name. |

This object MAY be extended with Specification Extensions.

#### Payload Object

Describes a single event payload.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| description | string | A brief description of the payload. This could contain examples of use. [CommonMark syntax](https://spec.commonmark.org/) MAY be used for rich text representation. |
| content\_type | string | **REQUIRED**. The media type of the payload. |
| content | string | The schema defining the content of the payload. |

This object MAY be extended with Specification Extensions.

#### Integration Object

Holds a reference to external integration used by component.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| name | string | **REQUIRED.** Name of the integration. |
| description | string | A short description of the integration. [CommonMark syntax](https://spec.commonmark.org/) MAY be used for rich text representation. |
| usage | string | List of capabilities delivered by integration. |
| direction | string | Direction of the integration. Allowed values are "out" for outgoing integration (i.e. external api used), "in" for incoming (i.e. api exposed), or "bi" for bidirectional. |
| eventsEmitted | \[EventEmitted Object\] | A list of events throwed by component. |
| eventsConsumed | \[EventsConsumed Object\] | A list of events consumed by component. |
| depends_on | string | A list of component names consumed by component. |
| integrations | \[Integration Object\] | A list of external integrations used by component. |
| tags | string | A list of tag names. |
| layer | string | Layer of the system. |

This object MAY be extended with Specification Extensions.

#### External Documentation Object

Allows referencing an external resource for extended documentation.

##### Fixed Fields

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| description | string | A short description of the target documentation. [CommonMark syntax](https://spec.commonmark.org/) MAY be used for rich text representation. |
| url | string | **REQUIRED**. The URL for the target documentation. Value MUST be in the format of a URL. |

This object MAY be extended with Specification Extensions.

### Specification Extensions

While the OpenHLA Specification tries to accommodate most use cases,
additional data can be added to extend the specification at certain
points.

The extensions properties are implemented as patterned fields that are
always prefixed by \"x-\".

| Field Pattern | Type | Description |
| ---------- | ---- | ----------- |
| \^x- | Any | Allows extensions to the OpenHLA Schema. The field name MUST begin with x-, for example, x-internal-id. The value can be null, a primitive, an array or an object. Can have any valid JSON format value. |

The extensions may or may not be supported by the available tooling, but
those may be extended as well to add requested support (if tools are
internal or open-sourced).
